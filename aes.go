package main

import (
	"bytes"
	"crypto/aes"
	"crypto/cipher"
	"encoding/base64"
	"math/rand"
	"time"

	"errors"
)

const (
	KEY_LEN = 32
	IV_LEN  = 16
)

//ErrParamNil .
var ErrParamNil = errors.New("param not nil")

const letterBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
const (
	letterIdxBits = 6                    // 6 bits to represent a letter index
	letterIdxMask = 1<<letterIdxBits - 1 // All 1-bits, as many as letterIdxBits
	letterIdxMax  = 63 / letterIdxBits   // # of letter indices fitting in 63 bits
)

var src = rand.NewSource(time.Now().UnixNano())

func RandBytesByNum(n int) []byte {
	b := make([]byte, n)
	// A src.Int63() generates 63 random bits, enough for letterIdxMax characters!
	for i, cache, remain := n-1, src.Int63(), letterIdxMax; i >= 0; {
		if remain == 0 {
			cache, remain = src.Int63(), letterIdxMax
		}
		if idx := int(cache & letterIdxMask); idx < len(letterBytes) {
			b[i] = letterBytes[idx]
			i--
		}
		cache >>= letterIdxBits
		remain--
	}
	return b
}

//RandSeq .
func RandSeq(keyLen int, ivLen int) ([]byte, []byte) {
	return RandBytesByNum(keyLen), RandBytesByNum(ivLen)
}

//AesEncrypt .
func AesEncrypt(origData []byte, key []byte, iv []byte) (string, error) {
	if len(key) == 0 || len(iv) == 0 || len(origData) == 0 {
		return "", ErrParamNil
	}

	block, err := aes.NewCipher([]byte(key))
	if err != nil {
		return "", err
	}

	blockSize := block.BlockSize()
	origData = PKCS5Padding1(origData, blockSize)
	blockMode := cipher.NewCBCEncrypter(block, []byte(iv))
	crypted := make([]byte, len(origData))

	blockMode.CryptBlocks(crypted, origData)
	return base64.StdEncoding.EncodeToString(crypted), nil
}

//AesDecrypt .
func AesDecrypt(crypted []byte, key []byte, iv []byte) (string, error) {
	if len(key) == 0 || len(iv) == 0 || len(crypted) == 0 {
		return "", ErrParamNil
	}

	decodeData := make([]byte, base64.StdEncoding.DecodedLen(len(crypted)))
	_, err := base64.StdEncoding.Decode(decodeData, crypted)
	if err != nil {
		return "", err
	}

	block, err := aes.NewCipher([]byte(key))
	if err != nil {
		return "", err
	}

	blockMode := cipher.NewCBCDecrypter(block, []byte(iv))
	origData := make([]byte, len(decodeData))
	blockMode.CryptBlocks(origData, decodeData)
	origData = PKCS5UnPadding1(origData)
	return string(origData), nil
}

//PKCS5Padding1 .
func PKCS5Padding1(ciphertext []byte, blockSize int) []byte {
	padding := blockSize - len(ciphertext)%blockSize
	padtext := bytes.Repeat([]byte{byte(padding)}, padding)
	return append(ciphertext, padtext...)
}

//PKCS5UnPadding1 .
func PKCS5UnPadding1(origData []byte) []byte {
	length := len(origData)
	unpadding := int(origData[length-1])
	return origData[:(length - unpadding)]
}

func EncodeBytes(src []byte) []byte {
	encodeBytes := make([]byte, base64.StdEncoding.EncodedLen(len(src)))
	base64.StdEncoding.Encode(encodeBytes, src)
	return encodeBytes
}

func DecodeBytes(src []byte) ([]byte, error) {
	decodeBytes := make([]byte, base64.StdEncoding.DecodedLen(len(src)))
	_, err := base64.StdEncoding.Decode(decodeBytes, src)
	if err != nil {
		return nil, err
	}
	return decodeBytes, nil
}
