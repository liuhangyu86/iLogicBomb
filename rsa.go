package main

import (
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"encoding/pem"
	"errors"
)

var (
	privateKey = []byte(`
-----BEGIN RSA PRIVATE KEY-----
MIICXAIBAAKBgQC96WTS6hDTzuisbFz5Bczl8dDhqpAMjt/D8ps1lxvWxMj8ROET
mbK5xyQ1omnNByPO8ag5KlKwPV8qsL7nZ3QuF6NTCVTX6c1ao/3NyGS0NMdoyzef
lXPATFFNBtDiAYJiReaB0vjJKp2V8BlUiqtSn0SYkRryBSqQMQQlePcF4QIDAQAB
AoGBALnL/lUOpFnkguQ1vBvxXc3Vb6toCDgRC8CTuN+ML7NFoPCVnrJjW2S//J/w
fpwVESZkbFVJH87GrDWC57JTF2hVTQL+uZMd2ZqkX3vjAL/wFxomE85Z/qKMKh08
3m2206C0Jr1Dg0zjevhCezGxaqByCG8FMr7ku59axEDLVBEBAkEA3UATF1fXQkCl
DgguAHxXyk/guaplDX6cBwSvTZscH1Rl4xgeKjrve6ht/mKgqQXTTLiilYxRKDMx
2j0YgOTkUQJBANu9RaPRIaLxdIZZ+8+KBpPTv/5c6UfJdZlMkAZLYhW+drD2KK+a
UVNZhIFTSKGM5Rb65+TU5UKcQ0FDL3vNdJECQFep3qaeIGVY1c1Egm2g1uwLUEBg
el+pbcr+GTOHpNHpD+G2GaWj6mW1q7difsPZ0goREiJtEW77MeKF2KKhjrECQBgZ
xk2VXm8UtIcXz3qP1PDZxLENV7i7694Ie7N1CSkBXttB5Gx+LHVL2DCnBcxSO74t
fsmmYA7SPL4ntEmL20ECQEBylFI055iISkqnQPu0/7t+yl/7Bc8pnFHFEI/JerpU
FR7PEks1SwjTIFeD3Ovt6Obns9qWG6gZKQtQyBT+lEs=
-----END RSA PRIVATE KEY-----
`)

	publicKey = []byte(`
-----BEGIN PUBLIC KEY-----
MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQC96WTS6hDTzuisbFz5Bczl8dDh
qpAMjt/D8ps1lxvWxMj8ROETmbK5xyQ1omnNByPO8ag5KlKwPV8qsL7nZ3QuF6NT
CVTX6c1ao/3NyGS0NMdoyzeflXPATFFNBtDiAYJiReaB0vjJKp2V8BlUiqtSn0SY
kRryBSqQMQQlePcF4QIDAQAB
-----END PUBLIC KEY-----
`)
)

// RsaEncrypt 加密
func RsaEncrypt(origData []byte) ([]byte, error) {
	block, _ := pem.Decode(publicKey)
	if block == nil {
		return nil, errors.New("public key error")
	}
	pubInterface, err := x509.ParsePKIXPublicKey(block.Bytes)
	if err != nil {
		return nil, err
	}
	pub := pubInterface.(*rsa.PublicKey)
	return rsa.EncryptPKCS1v15(rand.Reader, pub, origData)
}

//RsaDecrypt 解密
func RsaDecrypt(ciphertext []byte) ([]byte, error) {
	block, _ := pem.Decode(privateKey)
	if block == nil {
		return nil, errors.New("private key error!")
	}
	priv, err := x509.ParsePKCS1PrivateKey(block.Bytes)
	if err != nil {
		return nil, err
	}
	return rsa.DecryptPKCS1v15(rand.Reader, priv, ciphertext)
}
